#!/usr/bin/env python
# -*- encoding: iso-8859-1 -*-
"""
Generate workflows view

Author: Dan Gunter <dkgunter@lbl.gov>
Created: 14 October 2011
"""
__rcsid__ = "$Id$"

import datetime
from operator import itemgetter
import logging

import pymongo

# Logging
g_log = logging.getLogger("matproj.wf")

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# M/R framework functions

g_tasks = { }

def aoc(dct, key, value):
    """Append-or-create.
    Append to list at dct[key], or create new list.
    """
    if dct.has_key(key):
        dct[key].append(value)
    else:
        dct[key] = [value]
        
def setup(db='', host='', user='', passwd='', **kw):
    global g_tasks
    dbconn = pymongo.Connection(host)[db]
    if user and passwd:
       dbconn.authenticate(user, passwd)
    tasks_coll = dbconn.tasks
    # slurp up tasks
    for doc in tasks_coll.find(spec={}, fields=('task_id',
                                                'crystal_id',
                                                'was_stopped',
                                                'was_successful',
                                                'updated_at',
                                                'is_hubbard')):
        task_id = doc['task_id']
        del doc['task_id']
        aoc(g_tasks, task_id, doc)        

def mapper(doc):
    return doc['crystal_id']

fields = ['engine_id', 'state', 'failed', 'created_at', 'crystal_id']

# create these extraction functions only once
get_created_at = itemgetter('created_at')
get_eid = itemgetter('engine_id')

def reducer(values):
    xtl_id = values[0]['crystal_id']
    # sort by created_at so the workflow is built in time-order
    values.sort(key=get_created_at)
    # build workflow obj
    wf = build_xtl_workflow(xtl_id, values, g_tasks)
    # set result to contain some metadata and various
    # representations of the workflow obj
    result = {
        'crystal_id' : xtl_id,
        'engine_ids' : map(get_eid,values),
        'repr' : {
            'dot' : wf.as_dot(),
            'd3' : wf.as_d3(),
            }
        }
    return result

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    
# Constants
# ---------
# States
CR, BL, RN, ST, KL, AB, CM = (
    "created", "building", "running",
    "stopped", "killed", "abyss", "completed")
# State transitions (links from child to parent)
parents = { BL : CR,
            RN : BL,
            KL : RN,
            AB : RN,
            CM : RN,
            ST : RN }
# Maximum time since a task was updated
# before we consider it dead.
MAX_TASK_IDLE = 60*60*8

def build_xtl_workflow(xtl_id,  engines, tasks):
    """Build the crystal workflow obj. using the engine and task collections.
    """
    # init workflow obj
    xtl_wf = Workflow(root = "Crystal_{0:d}".format(xtl_id))
    # for each engine-id, retrieve associated info from task table
    # and append as a sub-workflow to the workflow
    for i, eng in enumerate(engines):
        eid = eng['engine_id']
        task_list = tasks.get(eid, [ ]) 
        if not task_list:
            # XXX: What is the right thing to do here?
            g_log.warn("tasks.missing, engine_id={0:d}".format(eid))
            continue
        # Find most recent hubbard and non-hubbard tasks.
        keys = ('hubbard', 'non_hubbard')
        recent = dict.fromkeys(keys)
        for task in task_list:
            is_hubbard = bool(task['is_hubbard'])
            key = keys[is_hubbard]
            if (recent[key] is None or
                recent[key]['updated_at'] < task['updated_at']):
                recent[key] = task
        # Add workflows for these tasks
        for key in keys:
            if recent[key]:
                state = determine_state(eng['state'], task)
                xtl_wf.add((key,))
                sfx = {'hubbard':'GGA+U', 'non_hubbard':'GGA'}[key]
                xtl_wf.add_subwf(state, suffix=sfx)
    # that's it, return the workflow for this crystal
    return xtl_wf


class Workflow:
    """Graph of states.

    The tree is represented internally as a nested sequence.
    For example, if the tree is:
      a
     /\ \
    b  c f
       /\
      d  e

   The list will be: (a, (b,), (c, (d, e)), (f,))
    """
    def __init__(self, root="start"):
        """Create with a root node.
        """
        self._tree = [root]
        self._nodes = 1
        
    def __len__(self):
        return self._nodes
    
    def add(self, value):
        """Add a sub-tree, pointed to by root.
        """
        self._nodes += len(value)
        self._tree.append(value)

    def add_subwf(self, state, suffix=""):
        """Add a subwf, off the root, for the given current state.
        """
        subwf = nest(path(state, suffix=suffix))
        self.add(subwf)

    def chain_subwf(self, state, item, suffix=""):
        """Add a subwf, off the i-th item, for the given
        current state.
        """
        subwf = nest(path(state, suffix=suffix))
        self._nodes += len(subwf)
        item.append(subwf)

    def __add__(self, other):
        self.add(other._tree)
        return(self)
        
    def as_dot(self):
        """Represent for graphviz.

        Returns:
           (str) Graphviz commands
        """
        def _format(parent, child, links):
             if parent:
                 links.append("{0} -> {1};".format(parent, child))
        links = [ ]
        traverse(None, self._tree, _format, (links,))
        result = ("digraph workflow {\n"
                  "rankdir=LR;\n" +
                  '\n'.join(links) +
                  "}\n")
        return result

    def as_d3(self):
        """Represent as JSON for D3 graphing.
        Wrapper around recursive function as_d3()

        Returns:
           (dict) Nested dictionary/sequences        
        """
        return as_d3(self._tree)[0]

def as_d3(cur):
    """Represent as JSON for D3 graphing.

    Desired format is::
    { "name": NODE-NAME,
       "children": [
            ..list of nodes and their children
            ],
        ..etc..
    }

    For example:

    >>> from pprint import pprint
    >>> c = ['start', ('1', ['created', 'building', 'running', 'killed']), ('2', ['created', 'building', 'running', 'abyss']), 'final']
    >>> pprint(as_d3(c))
    [{'children': [{'children': [{'name': 'created'},
                                 {'name': 'building'},
                                 {'name': 'running'},
                                 {'name': 'killed'}],
                    'name': '1'},
                   {'children': [{'name': 'created'},
                                 {'name': 'building'},
                                 {'name': 'running'},
                                 {'name': 'abyss'}],
                    'name': '2'}],
      'name': 'start'},
     {'name': 'final'}]

    Args:
       cur - List of strings/lists at current depth in tree
    Returns:
       (dict) Nested dictionary/sequences
    """
    result = [ ]
    prev_terminal = None
    for node in cur:
        if isinstance(node, str):
            result.append({"name":node})
            prev_terminal = node
        else:
            if not prev_terminal:
                raise ValueError("bad tree structure")
            r = result[-1]
            if not r.has_key('children'):
                r['children'] = [ ]
            r['children'].extend(as_d3(node))
    return result

def determine_state(engine_state, task):
    if engine_state == CR:
        return CR
    if engine_state == RN:
        if not task['was_successful']:
            return KL
        elif updated_sec(task) > MAX_TASK_IDLE:
            return AB
        else:
            return RN
    if engine_state == CM:
        if task['was_stopped']:
            return ST
        else:
            return CM
    raise ValueError("Unexpected engine state: " + engine_state)

def updated_sec(task):
    now = datetime.datetime.now()
    return (now - task['updated_at']).seconds

def subwf(state, **kw):
    return nest(path(state, **kw))

def path(state, suffix=''):
    """Make list of all states up to and including input state.

    Args:
      state - (string) name of a state.
    Returns:
      list of all states up to and including input state
      
    For example:
    >>> path('foo')
    ['foo']
    >>> path('killed')
    ['created', 'building', 'running', 'killed']
    """
    p = parents.get(state, None)
    state_name = (state, state + suffix)[bool(suffix)]
    if p is None:
        return [state_name]
    else:
        return path(p, suffix=suffix) + [state_name]
        
def nest(states):
    """Nest a list of strings, so that it fits the tree structure
    expected in Workflow.

    Args:
       states - list of string
    Returns:
       nested list with same strings
       
    For example,
    >>> nest(["start", CR, BL, RN])
    ['start', ['created', ['building', ['running']]]]
    """    
    if len(states) == 1:
        return states
    else:
        return [states[0], nest(states[1:])]

def traverse(prev, cur, func, args):
    """LR depth-first traversal of a list (iterable) representing a tree.
   """
    prev_terminal = None
    for node in cur:
        if isinstance(node, str):
            func(prev, node, *args)
            prev_terminal = node
        else:
            if not prev_terminal:
                raise ValueError("bad tree structure")
            traverse(prev_terminal, node, func, args)

# Run doctests
if __name__ == "__main__":
    import doctest
    doctest.testmod()
