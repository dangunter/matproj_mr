import web, sys

urls = (
    '/(.*)', 'default'
)
app = web.application(urls, globals())

class default: 
    def GET(self, name):
        if not name: 
            return ''
        try:
            slurp = ''.join(file(name).readlines())
        except IOError:
            slurp = ''
        if name.endswith(".json"):
            web.header('Content-Type', 'application/json')
        elif name.endswith(".js"):
            web.header('Content-Type', 'application/javascript')
        return slurp

if __name__ == "__main__":
    app.run()
